package com.example.loanservice.exception;

import com.example.loanservice.exception.custom.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    @ExceptionHandler(LoanNotFoundException.class)
    public final ResponseEntity<?> handleLoanNotFoundException(LoanNotFoundException e) {
        logError(e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler(RepaymentScheduleNotFoundException.class)
    public final ResponseEntity<?> handleRepaymentScheduleNotFoundException(RepaymentScheduleNotFoundException e) {
        logError(e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler(RepaymentAlreadyPaidException.class)
    public final ResponseEntity<?> handleRepaymentAlreadyPaidException(RepaymentAlreadyPaidException e) {
        logError(e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler(RepaymentAmountNotMatchException.class)
    public final ResponseEntity<?> handleRepaymentAmountNotMatchException(RepaymentAmountNotMatchException e) {
        logError(e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler(LoanAlreadyPaidException.class)
    public final ResponseEntity<?> handleLoanAlreadyPaidException(LoanAlreadyPaidException e) {
        logError(e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler(LoanOwnerException.class)
    public final ResponseEntity<?> handleLoanOwnerException(LoanOwnerException e) {
        logError(e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<?> handleAllExceptions(Exception e) {
        logError(e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }


    private void logError(Exception e) {
        String message = e.getCause() == null ? e.getMessage() : e.getMessage() + System.lineSeparator() + e.getLocalizedMessage();

        LOGGER.error(message);
    }
}
