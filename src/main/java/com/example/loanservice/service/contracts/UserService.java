package com.example.loanservice.service.contracts;

import com.example.loanservice.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User getUserByEmail(String email);

}
