package com.example.loanservice.repository;

import com.example.loanservice.model.Loan;
import com.example.loanservice.model.RepaymentsSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepaymentScheduleRepository extends JpaRepository<RepaymentsSchedule, Long> {

    @Query("SELECT rs FROM RepaymentsSchedule rs WHERE rs.repaymentsScheduleId = " +
            "(SELECT MIN(rs2.repaymentsScheduleId) FROM RepaymentsSchedule rs2 where  rs2.loan = ?1 AND rs2.paidOnDate IS NULL)")
    Optional<RepaymentsSchedule> findNotPaidSchedules(Loan loan);

    void deleteByLoan(Loan loan);
}
