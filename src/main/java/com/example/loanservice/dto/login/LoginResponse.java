package com.example.loanservice.dto.login;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class LoginResponse {

    private String token;
}
