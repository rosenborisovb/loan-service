package com.example.loanservice.dto.repayment;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Data
public class RepaymentRequest {

    @NotNull
    private Long loanId;

    @NotNull
    private BigDecimal repaymentAmount;

}
