package com.example.loanservice.service;

import com.example.loanservice.exception.custom.LoanAlreadyPaidException;
import com.example.loanservice.exception.custom.RepaymentAlreadyPaidException;
import com.example.loanservice.exception.custom.RepaymentScheduleNotFoundException;
import com.example.loanservice.model.Loan;
import com.example.loanservice.model.RepaymentsSchedule;
import com.example.loanservice.repository.RepaymentScheduleRepository;
import com.example.loanservice.service.contracts.RepaymentScheduleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.loanservice.helper.Constants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RepaymentScheduleServiceTest {

    public static final long ID = 123L;
    public static final BigDecimal REPAYMENT_AMOUNT = BigDecimal.valueOf(3);
    private RepaymentScheduleRepository repaymentScheduleRepository;

    private RepaymentScheduleService repaymentScheduleService;

    @BeforeEach
    public void setUp() {
        this.repaymentScheduleRepository = Mockito.mock(RepaymentScheduleRepository.class);
        this.repaymentScheduleService = new RepaymentScheduleServiceImpl(repaymentScheduleRepository);
    }

    @Test
    public void whenCorrectLoanIsProvided_thenRepaymentScheduleIsCreated() {
        // GIVEN
        Loan loanMock = buildLoan();
        List<RepaymentsSchedule> repaymentsSchedulesMock = buildRepaymentScheduleList();

        // WHEN
        List<RepaymentsSchedule> result = repaymentScheduleService.createRepaymentsSchedule(loanMock);

        // THEN
        assertNotNull(result);
        assertEquals(repaymentsSchedulesMock.size(), result.size());
        for (int i = 0; i < result.size(); i++) {
            assertEquals(repaymentsSchedulesMock.get(i).getMaturityDate(), result.get(i).getMaturityDate());
            assertEquals(repaymentsSchedulesMock.get(i).getNumberOfRepayment(), result.get(i).getNumberOfRepayment());
            assertEquals(repaymentsSchedulesMock.get(i).getPrincipalAmount(), result.get(i).getPrincipalAmount());
            assertEquals(repaymentsSchedulesMock.get(i).getRepaymentAmount(), result.get(i).getRepaymentAmount());
            assertEquals(repaymentsSchedulesMock.get(i).getPrincipalAmountRepayment(), result.get(i).getPrincipalAmountRepayment());
            assertEquals(repaymentsSchedulesMock.get(i).getInterestAmountRepayment(), result.get(i).getInterestAmountRepayment());
        }
    }

    @Test
    public void whenCorrectRepaymentScheduleIdAndAmountAreProvided_thenSuccessfulRepaid() {
        // GIVEN
        RepaymentsSchedule repaymentsScheduleMock = buildRepaymentSchedule(null, REPAYMENT_AMOUNT);
        RepaymentsSchedule repaymentsScheduleAfterPaidMock = buildRepaymentSchedule(LocalDateTime.now(), REPAYMENT_AMOUNT);

        when(repaymentScheduleRepository.findById(anyLong())).thenReturn(Optional.of(repaymentsScheduleMock));
        when(repaymentScheduleRepository.save(any(RepaymentsSchedule.class))).thenReturn(repaymentsScheduleAfterPaidMock);

        // WHEN
        RepaymentsSchedule result = repaymentScheduleService.repay(ID, REPAYMENT_AMOUNT);

        // THEN
        assertNotNull(result);
        assertNotNull(result.getPaidOnDate());

    }

    @Test
    public void whenWrongRepaymentScheduleIdIsProvided_thenExceptionIsThrown() {
        // GIVEN
        when(repaymentScheduleRepository.findById(anyLong())).thenReturn(Optional.empty());

        // WHEN
        Exception exception = assertThrows(
                RepaymentScheduleNotFoundException.class, () -> {
                    repaymentScheduleService.repay(ID, REPAYMENT_AMOUNT);
                });
        assertEquals(String.format(REPAYMENT_SCHEDULE_DOES_NOT_EXISTS, ID), exception.getMessage());
    }

    @Test
    public void whenTryingToPayAlreadyPaidLoanSchedule_thenThrowAnException() {
        // GIVEN
        RepaymentsSchedule repaymentsScheduleMock = buildRepaymentSchedule(LocalDateTime.now(), REPAYMENT_AMOUNT);

        when(repaymentScheduleRepository.findById(anyLong())).thenReturn(Optional.of(repaymentsScheduleMock));

        // WHEN
        Exception exception = assertThrows(
                RepaymentAlreadyPaidException.class, () -> {
                    repaymentScheduleService.repay(ID, REPAYMENT_AMOUNT);
                });
        assertEquals(String.format(REPAYMENT_WITH_ID_IS_ALREADY_PAID, ID), exception.getMessage());
    }

    @Test
    public void whenTryToPayDifferentAmount_thenExceptionIsThrown() {
        // GIVEN
        RepaymentsSchedule repaymentsScheduleMock = buildRepaymentSchedule(null, REPAYMENT_AMOUNT);

        when(repaymentScheduleRepository.findById(anyLong())).thenReturn(Optional.of(repaymentsScheduleMock));

        // WHEN
        Exception exception = assertThrows(
                RepaymentScheduleNotFoundException.class, () -> {
                    repaymentScheduleService.repay(ID, BigDecimal.TEN);
                });
        assertEquals(String.format(AMOUNT_DOES_NOT_MATCH, BigDecimal.TEN, REPAYMENT_AMOUNT, REPAYMENT_AMOUNT), exception.getMessage());
    }

    @Test
    public void  whenCorrectLoanForRepaymentIsProvided_thenRepaymentIsMade() {
        // GIVEN
        Loan loanMock = buildLoan();
        RepaymentsSchedule repaymentsSchedulesListMock = buildRepaymentSchedule(null, REPAYMENT_AMOUNT);
        RepaymentsSchedule repaymentsScheduleMock = buildRepaymentSchedule(LocalDateTime.now(), REPAYMENT_AMOUNT);

        when(repaymentScheduleRepository.findNotPaidSchedules(any(Loan.class))).thenReturn(Optional.of(repaymentsSchedulesListMock));
        when(repaymentScheduleRepository.save(any(RepaymentsSchedule.class))).thenReturn(repaymentsScheduleMock);

        // WHEN
        RepaymentsSchedule result = repaymentScheduleService.repay(loanMock);

        assertNotNull(result);
        assertNotNull(result.getPaidOnDate());
    }

    @Test
    public void whenAlreadyPaidLoanIsProvided_thenExceptionIsThrown() {
        // GIVEN
        Loan loanMock = buildLoan();
        loanMock.setLoanId(ID);

        when(repaymentScheduleRepository.findNotPaidSchedules(any(Loan.class))).thenReturn(Optional.empty());

        // WHEN
        Exception exception = assertThrows(
                LoanAlreadyPaidException.class, () -> {
                    repaymentScheduleService.repay(loanMock);
                });
        assertEquals(String.format(LOAN_ALREADY_PAID, ID), exception.getMessage());
    }



    private RepaymentsSchedule buildRepaymentSchedule(LocalDateTime localDateTime, BigDecimal amount) {
        return RepaymentsSchedule.builder()
                .repaymentsScheduleId(ID)
                .numberOfRepayment(1)
                .principalAmount(BigDecimal.TEN)
                .repaymentAmount(BigDecimal.valueOf(3))
                .principalAmountRepayment(amount)
                .interestAmountRepayment(BigDecimal.ONE)
                .paidOnDate(localDateTime)
                .build();
    }

    private List<RepaymentsSchedule> buildRepaymentScheduleList() {
        List<RepaymentsSchedule> result = new ArrayList<>();
        double[] principleAmount = {92.12, 84.16, 76.12, 68.00, 59.80, 51.52, 43.16, 34.71, 26.18, 17.56, 8.86, 0.00};
        double[] principleAmountRepayment = {7.88, 7.96, 8.04, 8.12, 8.20, 8.28, 8.36, 8.45, 8.53, 8.62, 8.70, 8.86};
        double[] interestAmountRepayment = {1.00, 0.92, 0.84, 0.76, 0.68, 0.60, 0.52, 0.43, 0.35, 0.26, 0.18, 0.09};
        double[] repaymentAmount = {8.88, 8.88, 8.88, 8.88, 8.88, 8.88, 8.88, 8.88, 8.88, 8.88, 8.88, 8.95};
        for (int i = 0; i < 12; i++) {
            result.add(RepaymentsSchedule.builder()
                    .maturityDate(LocalDate.now().plusMonths(i + 1))
                    .numberOfRepayment(i + 1)
                    .principalAmount(BigDecimal.valueOf(principleAmount[i]).setScale(2))
                    .repaymentAmount(BigDecimal.valueOf(repaymentAmount[i]).setScale(2))
                    .principalAmountRepayment(BigDecimal.valueOf(principleAmountRepayment[i]).setScale(2))
                    .interestAmountRepayment(BigDecimal.valueOf(interestAmountRepayment[i]).setScale(2))
                    .build());
        }
        return result;
    }

    private Loan buildLoan() {
        return Loan.builder()
                .principalAmount(BigDecimal.valueOf(100))
                .interestRate(BigDecimal.valueOf(12))
                .remainingPrincipalAmount(BigDecimal.valueOf(100))
                .loanDuration(12)
                .createdDay(LocalDateTime.now())
                .build();
    }
}
