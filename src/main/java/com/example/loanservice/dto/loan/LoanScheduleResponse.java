package com.example.loanservice.dto.loan;

import com.example.loanservice.dto.loan.LoanResponse;
import com.example.loanservice.model.RepaymentsSchedule;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoanScheduleResponse extends LoanResponse {
    private List<RepaymentsSchedule> repaymentSchedule;
}
