# Loan Service

Service that provides functionality for creating a loan and make payment on existing one

Currently provide two roles with different set of APIs

**User**
- Create a loan
- Get Loan by Id
- Get Loan Schedule By Loan Id
- Repay next one schedule

**Admin**
- Get loan By Id
- Get Loan Schedule By Loan Id
- Repay Specific Loan Schedule By Repayment Schedule Id
- Delete/Forgive Loan

The above resources are accessed through grant roles in the JWT.
JWT is created by the service when you provide valid credentials to Authentication endpoint.
It's open for everyone.

**Instruction to run locally**
- Clone the repository
- Build the application -> gradlew clean build
- Run the application -> gradlew run

**DB Credential**

http://localhost:8080/h2-console 

datasource.url=jdbc:h2:mem:loanservice_db

username: admin

password: admin
