package com.example.loanservice.exception.custom;

public class RepaymentAlreadyPaidException extends RuntimeException {
    public RepaymentAlreadyPaidException(String message) {
        super(message);
    }
}
