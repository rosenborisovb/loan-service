package com.example.loanservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "repayment_schedules")
public class RepaymentsSchedule {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
            name = "sequence-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "repayment_schedules_sequence"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "11"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "repayments_schedule_id")
    private Long repaymentsScheduleId;

    @NotNull
    @Column(name = "maturity_date")
    private LocalDate maturityDate;

    @NotNull
    @Column(name = "number_of_repayment")
    private Integer numberOfRepayment;

    @NotNull
    @Column(name = "principal_amount")
    private BigDecimal principalAmount;


    @Column(name = "repayment_amount")
    private BigDecimal repaymentAmount;

    @NotNull
    @Column(name = "principal_amount_repayment")
    private BigDecimal principalAmountRepayment;

    @NotNull
    @Column(name = "interest_amount_repayment")
    private BigDecimal interestAmountRepayment;

    @Column(name = "paid_on_date")
    private LocalDateTime paidOnDate;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id")
    @JsonIgnore
    private Loan loan;
}
