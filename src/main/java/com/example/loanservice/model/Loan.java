package com.example.loanservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "loans")
public class Loan {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
            name = "sequence-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "loans_sequence"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "11"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    @Column(name = "loan_id")
    private Long loanId;

    @NotNull
    @Column(name = "principal_amount")
    private BigDecimal principalAmount;

    @NotNull
    @Column(name = "interest_rate")
    private BigDecimal interestRate;

    @NotNull
    @Column(name = "remaining_principal_amount")
    private BigDecimal remainingPrincipalAmount;

    @NotNull
    @Column(name = "loan_duration")
    private Integer loanDuration;

    @NotNull
    @Column(name = "created_day")
    private LocalDateTime createdDay;

    @Nullable
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<RepaymentsSchedule> repaymentsSchedule;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

}
