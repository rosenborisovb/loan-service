package com.example.loanservice.exception.custom;

public class LoanAlreadyPaidException extends RuntimeException {
    public LoanAlreadyPaidException(String message) {
        super(message);
    }
}
