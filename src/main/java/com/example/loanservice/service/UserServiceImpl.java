package com.example.loanservice.service;

import com.example.loanservice.model.User;
import com.example.loanservice.repository.UserRepository;
import com.example.loanservice.service.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static com.example.loanservice.helper.Constants.USER_DOES_NOT_EXISTS;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return getUserByEmail(email);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(
                        () -> new UsernameNotFoundException(
                                String.format(USER_DOES_NOT_EXISTS, email)
                        )
                );
    }
}
