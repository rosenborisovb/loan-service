package com.example.loanservice.service;

import com.example.loanservice.dto.loan.LoanRequest;
import com.example.loanservice.dto.loan.LoanResponse;
import com.example.loanservice.dto.loan.LoanScheduleResponse;
import com.example.loanservice.dto.repayment.RepaymentRequest;
import com.example.loanservice.dto.repayment.RepaymentScheduleRequest;
import com.example.loanservice.exception.custom.LoanNotFoundException;
import com.example.loanservice.exception.custom.LoanOwnerException;
import com.example.loanservice.model.Loan;
import com.example.loanservice.model.RepaymentsSchedule;
import com.example.loanservice.model.User;
import com.example.loanservice.repository.LoanRepository;
import com.example.loanservice.service.contracts.LoanService;
import com.example.loanservice.service.contracts.RepaymentScheduleService;
import com.example.loanservice.service.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static com.example.loanservice.helper.Constants.LOAN_OWNER_NOT_MATCH;

@RequiredArgsConstructor
@Service
public class LoanServiceImpl implements LoanService {

    private static final String LOAN_WITH_ID_DOES_NOT_EXISTS = "Loan with id %s doesn't exists";
    private final RepaymentScheduleService repaymentScheduleService;

    private final LoanRepository loanRepository;

    private final UserService userService;

    @Override
    public LoanResponse createLoan(LoanRequest loanRequest, String email) {

        Loan loan = buildLoan(loanRequest);

        List<RepaymentsSchedule> repaymentsSchedules = repaymentScheduleService.createRepaymentsSchedule(loan);

        loan.setRepaymentsSchedule(repaymentsSchedules);

        User user = userService.getUserByEmail(email);
        loan.setUser(user);

        loan = loanRepository.save(loan);

        return buildLoanResponse(loan);
    }

    @Override
    public LoanResponse getLoanById(Long loanId, String email) {
        Loan loan = getLoan(loanId, email);

        return buildLoanResponse(loan);
    }

    @Override
    public LoanScheduleResponse getLoanSchedule(Long loanId, String email) {
        Loan loan = getLoan(loanId, email);

        return buildLoanScheduleResponse(loan);
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public LoanScheduleResponse makeRepaymentSchedule(RepaymentScheduleRequest repaymentScheduleRequest) {
        RepaymentsSchedule repaymentsSchedule = repaymentScheduleService
                .repay(repaymentScheduleRequest.getRepaymentScheduleId(), repaymentScheduleRequest.getRepaymentAmount());

        Loan loan = getLoan(repaymentsSchedule.getLoan().getLoanId());
        loan.setRemainingPrincipalAmount(loan.getRemainingPrincipalAmount().subtract(repaymentsSchedule.getPrincipalAmountRepayment()));

        return buildLoanScheduleResponse(loanRepository.save(loan));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public LoanScheduleResponse makeRepayment(RepaymentRequest repaymentRequest, String email) {

        Loan loan = getLoan(repaymentRequest.getLoanId(), email);

        RepaymentsSchedule repaymentsSchedule = repaymentScheduleService.repay(loan);

        loan.setRemainingPrincipalAmount(loan.getRemainingPrincipalAmount().subtract(repaymentsSchedule.getPrincipalAmountRepayment()));

        return buildLoanScheduleResponse(loanRepository.save(loan));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void deleteLoan(Long loanId) {
        Loan loan = getLoan(loanId);
        repaymentScheduleService.deleteRepaymentSchedulesForLoan(loan);

        loanRepository.delete(loan);
    }

    private Loan getLoan(Long loanId) {
        return loanRepository.findById(loanId).orElseThrow(() ->
                new LoanNotFoundException(String.format(LOAN_WITH_ID_DOES_NOT_EXISTS, loanId))
        );
    }

    private Loan getLoan(Long loanId, String email) {
        return loanRepository.findLoanByLoanIdAndUser_Email(loanId, email).orElseThrow(() ->
                new LoanOwnerException(String.format(LOAN_OWNER_NOT_MATCH, loanId))
        );
    }

    private static Loan buildLoan(LoanRequest loanRequest) {
        return Loan.builder()
                .principalAmount(loanRequest.getPrincipalAmount())
                .interestRate(loanRequest.getInterestRate())
                .remainingPrincipalAmount(loanRequest.getPrincipalAmount())
                .loanDuration(loanRequest.getLoanDuration())
                .createdDay(LocalDateTime.now())
                .build();
    }

    private static LoanResponse buildLoanResponse(Loan loan) {
        return LoanResponse.builder()
                .loanId(loan.getLoanId())
                .initialPrincipalAmount(loan.getPrincipalAmount())
                .remainingPrincipleAmount(loan.getRemainingPrincipalAmount())
                .interestRate(loan.getInterestRate())
                .createdOn(loan.getCreatedDay())
                .build();
    }

    private static LoanScheduleResponse buildLoanScheduleResponse(Loan loan) {
        return LoanScheduleResponse.builder()
                .loanId(loan.getLoanId())
                .initialPrincipalAmount(loan.getPrincipalAmount())
                .remainingPrincipleAmount(loan.getRemainingPrincipalAmount())
                .interestRate(loan.getInterestRate())
                .createdOn(loan.getCreatedDay())
                .repaymentSchedule(loan.getRepaymentsSchedule())
                .build();
    }
}
