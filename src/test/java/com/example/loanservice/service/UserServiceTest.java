package com.example.loanservice.service;

import com.example.loanservice.model.User;
import com.example.loanservice.repository.UserRepository;
import com.example.loanservice.service.contracts.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.example.loanservice.helper.Constants.USER_DOES_NOT_EXISTS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    public static final String EMAIL = "userEmail@email.com";
    private UserRepository userRepository;

    private UserService userService;

    @BeforeEach
    public void setUp() {
        this.userRepository = Mockito.mock(UserRepository.class);

        this.userService = new UserServiceImpl(userRepository);
    }

    @Test
    public void whenCorrectEmailIsProvided_thenUserIsRetrieved() {
        // GIVEN
        User userMock = buildUser();

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(userMock));

        // WHEN
        User result = userService.getUserByEmail(EMAIL);

        // THEN
        assertNotNull(result);
        assertEquals(userMock.getUserId(), result.getUserId());
        assertEquals(userMock.getEmail(), result.getEmail());
        assertEquals(userMock.getPassword(), result.getPassword());
    }

    @Test
    public void whenNotExistingEmilIsProvided_thenExceptionIsThrown() {
        // GIVEN
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        // WHEN
        Exception exception = assertThrows(
                UsernameNotFoundException.class, () -> {
                    userService.getUserByEmail(EMAIL);
                });
        assertEquals(String.format(USER_DOES_NOT_EXISTS, EMAIL), exception.getMessage());
    }

    private User buildUser() {
        return User.builder()
                .userId(123L)
                .firstName("FirstName")
                .lastName("LastName")
                .email(EMAIL)
                .password("password")
                .build();
    }
}
