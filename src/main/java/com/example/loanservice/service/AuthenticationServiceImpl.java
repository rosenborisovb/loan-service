package com.example.loanservice.service;

import com.example.loanservice.dto.login.LoginRequest;
import com.example.loanservice.dto.login.LoginResponse;
import com.example.loanservice.model.User;
import com.example.loanservice.security.JwTokenService;
import com.example.loanservice.service.contracts.AuthenticationService;
import com.example.loanservice.service.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;

    private final UserService userService;

    private final JwTokenService tokenService;

    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        authenticateUser(loginRequest);

        User user = userService.getUserByEmail(loginRequest.getEmail());

        return LoginResponse.builder()
                .token(tokenService.generateToken(user))
                .build();
    }

    private void authenticateUser(LoginRequest loginRequest) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
