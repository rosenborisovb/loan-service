package com.example.loanservice.exception.custom;

public class RepaymentAmountNotMatchException extends RuntimeException {
    public RepaymentAmountNotMatchException(String message) {
        super(message);
    }
}
