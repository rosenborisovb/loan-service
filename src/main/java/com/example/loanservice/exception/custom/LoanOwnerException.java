package com.example.loanservice.exception.custom;

public class LoanOwnerException extends RuntimeException {
    public LoanOwnerException(String message) {
        super(message);
    }
}
