package com.example.loanservice.service.contracts;

import com.example.loanservice.dto.login.LoginRequest;
import com.example.loanservice.dto.login.LoginResponse;

public interface AuthenticationService {

    LoginResponse login(LoginRequest loginRequest);
}
