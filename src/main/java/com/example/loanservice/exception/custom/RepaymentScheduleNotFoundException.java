package com.example.loanservice.exception.custom;

public class RepaymentScheduleNotFoundException extends RuntimeException {
    public RepaymentScheduleNotFoundException(String message) {
        super(message);
    }
}
