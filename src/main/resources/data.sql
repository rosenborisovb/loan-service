INSERT INTO roles (role_id, role) VALUES ('1', 'ADMIN');
INSERT INTO roles (role_id, role) VALUES ('2', 'USER');

INSERT INTO users(user_id, first_name, last_name, email, password) VALUES (1, 'Admin', 'Admin', 'admin@email.com', '$2a$10$Oy5Oq/mspdYQoR/q6x4.jOsEKOXpU3IqDWI.kSvu81WUP4HG/kT.W');
INSERT INTO users(user_id, first_name, last_name, email, password) VALUES (2, 'User', 'User', 'user@email.com', '$2a$10$Oy5Oq/mspdYQoR/q6x4.jOsEKOXpU3IqDWI.kSvu81WUP4HG/kT.W');


INSERT INTO user_role(user_id, role_id) VALUES (1, 1);
INSERT INTO user_role(user_id, role_id) VALUES (1, 2);
INSERT INTO user_role(user_id, role_id) VALUES (2, 2);