package com.example.loanservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class UserRoleId implements Serializable {

    private Long userId;

    private Long roleId;
}
