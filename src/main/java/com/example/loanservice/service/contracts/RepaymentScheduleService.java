package com.example.loanservice.service.contracts;

import com.example.loanservice.model.Loan;
import com.example.loanservice.model.RepaymentsSchedule;

import java.math.BigDecimal;
import java.util.List;

public interface RepaymentScheduleService {

    List<RepaymentsSchedule> createRepaymentsSchedule(Loan loan);

    RepaymentsSchedule repay(Long repaymentScheduleId, BigDecimal repaymentAmount);

    RepaymentsSchedule repay(Loan loan);

    void deleteRepaymentSchedulesForLoan(Loan loan);
}
