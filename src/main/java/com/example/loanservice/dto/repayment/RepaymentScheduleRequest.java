package com.example.loanservice.dto.repayment;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Data
public class RepaymentScheduleRequest {
    @NotNull
    private Long repaymentScheduleId;

    @NotNull
    private BigDecimal repaymentAmount;
}
