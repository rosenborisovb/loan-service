package com.example.loanservice.service;

import com.example.loanservice.exception.custom.LoanAlreadyPaidException;
import com.example.loanservice.exception.custom.RepaymentAlreadyPaidException;
import com.example.loanservice.exception.custom.RepaymentScheduleNotFoundException;
import com.example.loanservice.model.Loan;
import com.example.loanservice.model.RepaymentsSchedule;
import com.example.loanservice.repository.RepaymentScheduleRepository;
import com.example.loanservice.service.contracts.RepaymentScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.loanservice.helper.Constants.*;

@RequiredArgsConstructor
@Service
public class RepaymentScheduleServiceImpl implements RepaymentScheduleService {


    private static final int MONTHS_PER_YEAR = 12;
    private static final int NEXT_REPAYMENT_SCHEDULE = 0;
    private static final int PERCENT_DIVIDER = 100;
    private static final int SCALE_SIZE = 2;

    private final RepaymentScheduleRepository repaymentScheduleRepository;

    @Override
    public List<RepaymentsSchedule> createRepaymentsSchedule(Loan loan) {
        List<RepaymentsSchedule> repaymentsSchedules = new ArrayList<>();

        BigDecimal monthlyPayment =
                calculateMonthlyPayment(loan.getInterestRate(), loan.getPrincipalAmount(), loan.getLoanDuration())
                        .setScale(SCALE_SIZE, RoundingMode.HALF_UP);

        BigDecimal monthlyInterestMultiplier =
                loan.getInterestRate().divide(BigDecimal.valueOf(MONTHS_PER_YEAR)).divide(BigDecimal.valueOf(PERCENT_DIVIDER));

        LocalDate localDate = LocalDate.now();

        for (int i = 0; i < loan.getLoanDuration(); i++) {
            BigDecimal remainingPrincipalAmount = repaymentsSchedules.isEmpty()
                    ? loan.getPrincipalAmount().setScale(SCALE_SIZE, RoundingMode.HALF_UP)
                    : repaymentsSchedules.get(i - 1).getPrincipalAmount().setScale(SCALE_SIZE, RoundingMode.HALF_UP);;

            BigDecimal interestAmount = remainingPrincipalAmount.multiply(monthlyInterestMultiplier).setScale(SCALE_SIZE, RoundingMode.HALF_UP);;

            BigDecimal principalRepayment = monthlyPayment.subtract(interestAmount).setScale(SCALE_SIZE, RoundingMode.HALF_UP);;

            BigDecimal principalAmountAfterPayment = remainingPrincipalAmount.subtract(principalRepayment);

            if (i == loan.getLoanDuration() -1) {
                if (principalAmountAfterPayment.compareTo(BigDecimal.ZERO) > 0) {
                    principalRepayment = principalRepayment.add(principalAmountAfterPayment);
                    monthlyPayment = monthlyPayment.add(principalAmountAfterPayment);
                }
                principalAmountAfterPayment = BigDecimal.ZERO.setScale(2);
            }

            RepaymentsSchedule currentRepaymentSchedule = RepaymentsSchedule.builder()
                    .maturityDate(localDate.plusMonths(i + 1))
                    .numberOfRepayment(i + 1)
                    .principalAmount(principalAmountAfterPayment)
                    .repaymentAmount(monthlyPayment)
                    .principalAmountRepayment(principalRepayment)
                    .interestAmountRepayment(interestAmount)
                    .loan(loan)
                    .build();

            repaymentsSchedules.add(currentRepaymentSchedule);

        }

        return repaymentsSchedules;

    }

    @Override
    public RepaymentsSchedule repay(Long repaymentScheduleId, BigDecimal repaymentAmount) {
        RepaymentsSchedule repaymentsSchedule = getRepaymentSchedule(repaymentScheduleId);
        if (repaymentsSchedule.getPaidOnDate() != null) {
            throw new RepaymentAlreadyPaidException(
                    String.format(REPAYMENT_WITH_ID_IS_ALREADY_PAID, repaymentsSchedule.getRepaymentsScheduleId()));
        }

        if (repaymentAmount.compareTo(repaymentsSchedule.getRepaymentAmount()) != 0) {
            throw new RepaymentScheduleNotFoundException(
                    String.format(AMOUNT_DOES_NOT_MATCH,
                            repaymentAmount,
                            repaymentsSchedule.getRepaymentAmount(),
                            repaymentsSchedule.getRepaymentAmount()));
        }
        repaymentsSchedule.setPaidOnDate(LocalDateTime.now());

        return repaymentScheduleRepository.save(repaymentsSchedule);
    }

    public RepaymentsSchedule repay(Loan loan) {
        RepaymentsSchedule repaymentsSchedules = repaymentScheduleRepository.findNotPaidSchedules(loan)
                .orElseThrow(() -> new LoanAlreadyPaidException(
                        String.format(LOAN_ALREADY_PAID, loan.getLoanId())
                ));


        repaymentsSchedules.setPaidOnDate(LocalDateTime.now());

        return repaymentScheduleRepository.save(repaymentsSchedules);
    }

    @Override
    public void deleteRepaymentSchedulesForLoan(Loan loan) {
        repaymentScheduleRepository.deleteByLoan(loan);
    }

    private RepaymentsSchedule getRepaymentSchedule(Long repaymentScheduleId) {
        return repaymentScheduleRepository.findById(repaymentScheduleId).orElseThrow(
                () -> new RepaymentScheduleNotFoundException(
                        String.format(REPAYMENT_SCHEDULE_DOES_NOT_EXISTS, repaymentScheduleId)
                )
        );
    }

    private BigDecimal calculateMonthlyPayment(BigDecimal interestRate, BigDecimal principal, Integer duration) {

        BigDecimal monthlyRate = (interestRate.divide(BigDecimal.valueOf(PERCENT_DIVIDER))).divide(BigDecimal.valueOf(MONTHS_PER_YEAR));

        return ((BigDecimal.ONE.add(monthlyRate)).pow(duration).multiply(monthlyRate))
                .divide((BigDecimal.ONE.add(monthlyRate)).pow(duration).subtract(BigDecimal.ONE), RoundingMode.HALF_UP)
                .multiply(principal);
    }
}
