package com.example.loanservice.repository;

import com.example.loanservice.model.Loan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoanRepository extends JpaRepository<Loan, Long> {
    Optional<Loan> findLoanByLoanIdAndUser_Email(Long loanId, String email);
}
