package com.example.loanservice.dto.loan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoanResponse {
    private Long loanId;

    private BigDecimal initialPrincipalAmount;

    private BigDecimal remainingPrincipleAmount;

    private BigDecimal interestRate;

    private LocalDateTime createdOn;

}
