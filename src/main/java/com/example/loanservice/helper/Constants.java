package com.example.loanservice.helper;

public class Constants {

    public static final String LOAN_OWNER_NOT_MATCH = "You don't have loan with id %s";

    public static final String REPAYMENT_SCHEDULE_DOES_NOT_EXISTS = "Repayment schedule with id %s doesn't exists";

    public static final String REPAYMENT_WITH_ID_IS_ALREADY_PAID = "Repayment with id %s is already paid";

    public static final String AMOUNT_DOES_NOT_MATCH = "Amount that you want to pay %s doesn't match the schedule amount %s. Please pay %s";

    public static final String LOAN_ALREADY_PAID = "Loan with id %s already paid";

    public static final String USER_DOES_NOT_EXISTS = "User with email %s doesn't exists";




}
