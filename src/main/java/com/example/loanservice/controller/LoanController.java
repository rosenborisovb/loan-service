package com.example.loanservice.controller;

import com.example.loanservice.dto.loan.LoanRequest;
import com.example.loanservice.dto.repayment.RepaymentRequest;
import com.example.loanservice.dto.repayment.RepaymentScheduleRequest;
import com.example.loanservice.model.anotation.AuthorizeAdmin;
import com.example.loanservice.model.anotation.AuthorizeAdminAndUser;
import com.example.loanservice.model.anotation.AuthorizeUser;
import com.example.loanservice.service.contracts.LoanService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/loans")
public class LoanController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoanController.class);


    private final LoanService loanService;

    @AuthorizeUser
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createLoan(@RequestBody @Valid LoanRequest loanRequest, Authentication authentication) {
        LOGGER.info("Post request: createLoan");
        String email = getUserEmail(authentication);
        return ResponseEntity.status(HttpStatus.CREATED).body(loanService.createLoan(loanRequest, email));
    }

    @AuthorizeAdminAndUser
    @GetMapping(
            path = "/{loanId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLoanById(@PathVariable Long loanId, Authentication authentication) {
        LOGGER.info("Get request: getLoanById");
        String email = getUserEmail(authentication);
        return ResponseEntity.status(HttpStatus.OK).body(loanService.getLoanById(loanId, email));
    }

    @AuthorizeAdminAndUser
    @GetMapping(
            path = "/loan-schedule/{loanId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLoanSchedule(@PathVariable Long loanId, Authentication authentication) {
        LOGGER.info("Get request: getLoanSchedule");
        String email = getUserEmail(authentication);
        return ResponseEntity.status(HttpStatus.OK).body(loanService.getLoanSchedule(loanId, email));
    }

    @AuthorizeAdmin
    @PostMapping(
            value = "/repay-loan-schedule",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> repayLoanSchedule(@RequestBody @Valid RepaymentScheduleRequest repaymentScheduleRequest) {
        LOGGER.info("Post request: repayLoanSchedule");
        return ResponseEntity.status(HttpStatus.CREATED).body(loanService.makeRepaymentSchedule(repaymentScheduleRequest));
    }

    @AuthorizeUser
    @PostMapping(
            value = "/repay-schedule",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> repaySchedule(@RequestBody @Valid RepaymentRequest repaymentRequest, Authentication authentication) {
        LOGGER.info("Post request: repaySchedule");
        String email = getUserEmail(authentication);
        return ResponseEntity.status(HttpStatus.CREATED).body(loanService.makeRepayment(repaymentRequest, email));
    }

    @AuthorizeAdmin
    @DeleteMapping(path = "/{loanId}")
    public ResponseEntity<?> deleteLoan(@PathVariable Long loanId) {
        LOGGER.info("Delete request: deleteLoan");
        loanService.deleteLoan(loanId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    private static String getUserEmail(Authentication authentication) {
        return ((UserDetails) authentication.getPrincipal()).getUsername();
    }
}
