package com.example.loanservice.service;


import com.example.loanservice.dto.loan.LoanRequest;
import com.example.loanservice.dto.loan.LoanResponse;
import com.example.loanservice.dto.loan.LoanScheduleResponse;
import com.example.loanservice.dto.repayment.RepaymentRequest;
import com.example.loanservice.dto.repayment.RepaymentScheduleRequest;
import com.example.loanservice.exception.custom.LoanOwnerException;
import com.example.loanservice.model.Loan;
import com.example.loanservice.model.RepaymentsSchedule;
import com.example.loanservice.model.User;
import com.example.loanservice.repository.LoanRepository;
import com.example.loanservice.service.contracts.LoanService;
import com.example.loanservice.service.contracts.RepaymentScheduleService;
import com.example.loanservice.service.contracts.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.example.loanservice.helper.Constants.LOAN_OWNER_NOT_MATCH;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class LoanServiceTest {

    public static final long ID = 123L;
    public static final String EMAIL = "userEmail@email.com";
    private RepaymentScheduleService repaymentScheduleService;

    private UserService userService;

    private LoanRepository loanRepository;

    private LoanService loanService;

    @BeforeEach
    public void setUp() {
        this.repaymentScheduleService = Mockito.mock(RepaymentScheduleService.class);
        this.userService = Mockito.mock(UserService.class);
        this.loanRepository = Mockito.mock(LoanRepository.class);
        this.loanService = new LoanServiceImpl(repaymentScheduleService, loanRepository, userService);
    }

    @Test
    public void whenValidLoanRequestForCreatingLoanIsProvided_thenLoanIsCreated() {
        // GIVEN
        LoanRequest loanRequest = new LoanRequest();
        Loan loanMock = buildLoan();
        List<RepaymentsSchedule> repaymentsSchedulesMock = createRepaymentScheduleList();
        User userMock = Mockito.mock(User.class);

        when(repaymentScheduleService.createRepaymentsSchedule(any(Loan.class))).thenReturn(repaymentsSchedulesMock);
        when(userService.getUserByEmail(anyString())).thenReturn(userMock);
        when(loanRepository.save(any(Loan.class))).thenReturn(loanMock);

        // WHEN
        LoanResponse result = loanService.createLoan(loanRequest, EMAIL);

        // THEN
        assertLoan(loanMock, result);
    }

    @Test
    public void whenCorrectLoanIdAndEmailForGettingLoanByIdAreProvided_thenLoanIsRetrieved() {
        // GIVEN
        Loan loanMock = buildLoan();
        when(loanRepository.findLoanByLoanIdAndUser_Email(anyLong(), anyString())).thenReturn(Optional.ofNullable(loanMock));

        //WHEN
        LoanResponse result = loanService.getLoanById(ID, EMAIL);

        // THEN
        assertLoan(loanMock, result);

    }

    @Test
    public void whenTryingToRetrieveLoanThatIsNotBelongToYou_thenExceptionIsThrown() {
        // GIVEN
        when(loanRepository.findLoanByLoanIdAndUser_Email(anyLong(), anyString())).thenReturn(Optional.empty());

        // WHEN
        Exception exception = assertThrows(
                LoanOwnerException.class, () -> {
                    loanService.getLoanById(ID, EMAIL);
                });
        assertEquals(String.format(LOAN_OWNER_NOT_MATCH, ID), exception.getMessage());
    }

    @Test
    public void whenCorrectLoanIdAndEmailForGettingLoanScheduleAreProvided_thenLoanScheduleIsRetrieved() {
        // GIVEN
        Loan loanMock = buildLoan();
        loanMock.setRepaymentsSchedule(createRepaymentScheduleList());

        when(loanRepository.findLoanByLoanIdAndUser_Email(anyLong(), anyString())).thenReturn(Optional.of(loanMock));

        // WHEN
        LoanScheduleResponse result = loanService.getLoanSchedule(ID, EMAIL);

        // THEN
        assertLoanWithSchedule(loanMock, result);
    }

    @Test
    public void whenCorrectRepaymentScheduleIdAndAmountAreProvided_thenSuccessfulRepaymentIsMade() {
        // GIVEN
        RepaymentScheduleRequest request = buildRepaymentScheduleRequest();
        Loan loanMock = buildLoan();
        RepaymentsSchedule repaymentsScheduleMock = createRepaymentSchedule();
        repaymentsScheduleMock.setPaidOnDate(LocalDateTime.now());
        repaymentsScheduleMock.setLoan(loanMock);
        Loan loanAfterRepaymentMock = buildLoanAfterRepayment(repaymentsScheduleMock.getPrincipalAmountRepayment());
        loanAfterRepaymentMock.setRepaymentsSchedule(Collections.singletonList(repaymentsScheduleMock));

        when(repaymentScheduleService.repay(anyLong(), any(BigDecimal.class))).thenReturn(repaymentsScheduleMock);
        when(loanRepository.findById(anyLong())).thenReturn(Optional.ofNullable(loanMock));
        when(loanRepository.save(any(Loan.class))).thenReturn(loanAfterRepaymentMock);

        // WHEN
        LoanScheduleResponse result = loanService.makeRepaymentSchedule(request);

        // THEN
        assertLoanWithSchedule(loanAfterRepaymentMock, result);
    }

    @Test
    public void whenCorrectLoanIdAndAmountAreProvided_theSuccessfulPaymentIsMade() {
        // GIVEN
        RepaymentRequest request = buildRepaymentRequest();
        Loan loanMock = buildLoan();
        RepaymentsSchedule repaymentsScheduleMock = createRepaymentSchedule();
        Loan loanAfterRepaymentMock = buildLoanAfterRepayment(repaymentsScheduleMock.getPrincipalAmountRepayment());
        loanAfterRepaymentMock.setRepaymentsSchedule(Collections.singletonList(repaymentsScheduleMock));

        when(loanRepository.findLoanByLoanIdAndUser_Email(anyLong(), anyString())).thenReturn(Optional.of(loanMock));
        when(repaymentScheduleService.repay(any(Loan.class))).thenReturn(repaymentsScheduleMock);
        when(loanRepository.save(any(Loan.class))).thenReturn(loanAfterRepaymentMock);

        // WHEN
        LoanScheduleResponse result = loanService.makeRepayment(request, EMAIL);

        // THEN
        assertLoanWithSchedule(loanAfterRepaymentMock, result);
    }

    @Test
    public void whenTryingToRepayLoanThatIsNotYours_thenExceptionIsThrown() {
        // GIVEN
        RepaymentRequest request = buildRepaymentRequest();

        when(loanRepository.findLoanByLoanIdAndUser_Email(anyLong(), anyString())).thenReturn(Optional.empty());

        // WHEN
        Exception exception = assertThrows(
                LoanOwnerException.class, () -> {
                    loanService.makeRepayment(request, EMAIL);
                });

        assertEquals(String.format(LOAN_OWNER_NOT_MATCH, ID), exception.getMessage());
    }

    private void assertLoan(Loan expected, LoanResponse result) {
        assertNotNull(result);
        assertEquals(expected.getLoanId(), result.getLoanId());
        assertEquals(expected.getPrincipalAmount(), result.getInitialPrincipalAmount());
        assertEquals(expected.getRemainingPrincipalAmount(), result.getRemainingPrincipleAmount());
        assertEquals(expected.getInterestRate(), result.getInterestRate());
        assertEquals(expected.getCreatedDay(), result.getCreatedOn());
    }

    private void assertLoanWithSchedule(Loan expected, LoanScheduleResponse result) {
        assertLoan(expected, result);

        assertEquals(expected.getRepaymentsSchedule().get(0).getRepaymentsScheduleId(), result.getRepaymentSchedule().get(0).getRepaymentsScheduleId());
        assertEquals(expected.getCreatedDay(), result.getCreatedOn());
    }

    private RepaymentRequest buildRepaymentRequest() {
        return RepaymentRequest.builder()
                .loanId(ID)
                .repaymentAmount(BigDecimal.TEN)
                .build();
    }

    private RepaymentScheduleRequest buildRepaymentScheduleRequest() {
        return RepaymentScheduleRequest.builder()
                .repaymentScheduleId(ID)
                .repaymentAmount(BigDecimal.TEN)
                .build();
    }

    private Loan buildLoan() {
        return Loan.builder()
                .loanId(ID)
                .principalAmount(BigDecimal.TEN)
                .interestRate(BigDecimal.ONE)
                .remainingPrincipalAmount(BigDecimal.TEN)
                .loanDuration(3)
                .createdDay(LocalDateTime.now())
                .build();
    }

    private Loan buildLoanAfterRepayment(BigDecimal principalRepayment) {
        Loan currentLoan = buildLoan();
        currentLoan.setRemainingPrincipalAmount(currentLoan.getRemainingPrincipalAmount().subtract(principalRepayment));

        return currentLoan;
    }

    private List<RepaymentsSchedule> createRepaymentScheduleList() {

        return Collections.singletonList(createRepaymentSchedule());
    }

    private RepaymentsSchedule createRepaymentSchedule() {
        return RepaymentsSchedule.builder()
                .repaymentsScheduleId(ID)
                .maturityDate(LocalDate.now().plusMonths(1))
                .numberOfRepayment(1)
                .principalAmount(BigDecimal.TEN)
                .repaymentAmount(BigDecimal.valueOf(3))
                .principalAmountRepayment(BigDecimal.valueOf(2))
                .interestAmountRepayment(BigDecimal.ONE)
                .build();
    }
}
