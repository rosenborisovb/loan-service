package com.example.loanservice.service.contracts;

import com.example.loanservice.dto.loan.LoanRequest;
import com.example.loanservice.dto.loan.LoanResponse;
import com.example.loanservice.dto.loan.LoanScheduleResponse;
import com.example.loanservice.dto.repayment.RepaymentRequest;
import com.example.loanservice.dto.repayment.RepaymentScheduleRequest;

public interface LoanService {

    LoanResponse createLoan(LoanRequest loanRequest, String email);

    LoanResponse getLoanById(Long loanId, String email);

    LoanScheduleResponse getLoanSchedule(Long loanId, String email);

    LoanScheduleResponse makeRepaymentSchedule(RepaymentScheduleRequest repaymentScheduleRequest);

    LoanScheduleResponse makeRepayment(RepaymentRequest repaymentRequest, String email);

    void deleteLoan(Long loanId);

}
