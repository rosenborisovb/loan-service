package com.example.loanservice.dto.loan;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoanRequest {

    private Long userId;

    private BigDecimal principalAmount;

    private BigDecimal interestRate;

    private Integer loanDuration;

}
